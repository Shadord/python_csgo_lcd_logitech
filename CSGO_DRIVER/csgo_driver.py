from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import json
from CSGO_DRIVER.game_state_manager import GameStateManager
from CSGO_DRIVER.models.csgo_config import CSGOConfig
from CSGO_DRIVER.payload_parser import PayloadParser
from CSGO_DRIVER.logger import Logger


class CSGODriver(HTTPServer):
    config: CSGOConfig

    def __init__(self, config, RequestHandler, eventHandler):
        self.config = config
        self.gamestatemanager = GameStateManager(eventHandler)

        super(CSGODriver, self).__init__((self.config.host, self.config.port), RequestHandler)

        self.setup_log_file()
        self.payload_parser = PayloadParser()

    def setup_log_file(self):
        self.log_file = Logger(time.asctime())


class RequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        length = int(self.headers['Content-Length'])
        body = self.rfile.read(length).decode('utf-8')

        payload = json.loads(body)
        # Ignore unauthenticated payloads
        if not self.authenticate_payload(payload):
            return None

        self.server.log_file.log_event(time.asctime(), payload)
        self.server.payload_parser.parse_payload(payload, self.server.gamestatemanager)

        self.send_header('Content-type', 'text/html')
        self.send_response(200)
        self.end_headers()

    def authenticate_payload(self, payload):
        if 'auth' in payload and 'token' in payload['auth']:
            return payload['auth']['token'] == self.server.config.token
        else:
            return False

    def log_message(self, format, *args):
        """
        Prevents requests from printing into the console
        """
        return
