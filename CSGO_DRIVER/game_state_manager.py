from CSGO_DRIVER.models.map import Map
from CSGO_DRIVER.models.player import Player
from CSGO_DRIVER.models.round import Round


class GameStateManager:
    def __init__(self, eventHandler):
        self.gamestate = GameState(eventHandler)


class GameState:
    def __init__(self, eventHandler):
        self.map = Map()
        self.player = Player()
        self.round = Round()
        self.eventHandler = eventHandler

    def event_handler(self, type):
        self.eventHandler(type, self)

    def update_round_phase(self, phase):
        self.round.phase = phase
        self.event_handler('phase')

    def update_round_bomb(self, bomb):
        self.round.bomb = bomb
        self.event_handler('bomb')

    def update_round_win_team(self, win_team):
        self.round.win_team = win_team
        self.event_handler('win_team')

    def update_round_kills(self, kills):
        if self.player.state.round_kills != kills and kills != 0:
            self.player.state.round_kills = kills
            self.event_handler('kill')
