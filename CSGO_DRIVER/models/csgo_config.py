class CSGOConfig:
    def __init__(self, host, port, token):
        self.host = host
        self.port = port
        self.token = token

    def to_dict(self):
        dictionnary = {"host": self.host, "port": self.port, "token": self.token}
        return dictionnary

    @classmethod
    def from_dict(cls, data):
        return cls(data['host'], data['port'], data["token"])

    def __repr__(self):
        string = "CSGOConfig {" \
                 "host: " + self.host + \
                 ", port: " + str(self.port) + \
                 ", token: " + self.token + "}"
        return string
