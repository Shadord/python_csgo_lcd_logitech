import time
from ctypes import *
import os, itertools

# Constants
NAME = "GLCD SDK.py"
VERSION = "0.0.1"

# LCD types
TYPE_MONO = 1
TYPE_COLOR = 2

# LCD Monochrome buttons
MONO_BUTTON_0 = 0x00000001
MONO_BUTTON_1 = 0x00000002
MONO_BUTTON_2 = 0x00000004
MONO_BUTTON_3 = 0x00000008

# LCD Color buttons
COLOR_BUTTON_LEFT = 0x00000100
COLOR_BUTTON_RIGHT = 0x00000200
COLOR_BUTTON_OK = 0x00000400
COLOR_BUTTON_CANCEL = 0x00000800
COLOR_BUTTON_UP = 0x00001000
COLOR_BUTTON_DOWN = 0x00002000
COLOR_BUTTON_MENU = 0x00004000

# LCD Monochrome size
MONO_WIDTH = 160
MONO_HEIGHT = 43


class LogitechLCDSDK:
    def __init__(self, dll_path):
        self.dll_path = dll_path
        self._dll_file = CDLL(dll_path)

        self.lcd_init = self._dll_file['LogiLcdInit']
        self.lcd_init.restype = c_bool
        self.lcd_init.argtypes = (c_wchar_p, c_int)

        self.lcd_is_connected = self._dll_file['LogiLcdIsConnected']
        self.lcd_is_connected.restype = c_bool
        self.lcd_is_connected.argtypes = [c_int]

        self.lcd_button_is_pressed = self._dll_file['LogiLcdIsButtonPressed']
        self.lcd_button_is_pressed.restype = c_bool
        self.lcd_button_is_pressed.argtypes = [c_int]

        self.lcd_update = self._dll_file['LogiLcdUpdate']
        self.lcd_update.restype = None

        self.lcd_shutdown = self._dll_file['LogiLcdShutdown']
        self.lcd_shutdown.restype = None

        self.lcd_set_background = self._dll_file['LogiLcdMonoSetBackground']
        self.lcd_set_background.restype = c_bool
        self.lcd_set_background.argtypes = [c_ubyte * 6880]

        self.lcd_set_text = self._dll_file['LogiLcdMonoSetText']
        self.lcd_set_text.restype = c_bool
        self.lcd_set_text.argtypes = (c_int, c_wchar_p)

        self.game = None

    def connect(self, game) -> bool:
        self.game = game
        init_result = self.lcd_init(self.game, TYPE_COLOR + TYPE_MONO)
        print(time.asctime() + " - [LOGITECH_SDK] STARTED")
        return init_result
