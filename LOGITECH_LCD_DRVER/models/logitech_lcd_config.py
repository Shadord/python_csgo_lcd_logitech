class LogitechLCDConfig:
    def __init__(self, sdk_path):
        self.sdkPath = sdk_path

    def to_dict(self):
        dictionnary = {"sdkPath": self.sdkPath}
        return dictionnary

    @classmethod
    def from_dict(cls, data):
        return cls(data['sdkPath'])

    def __repr__(self):
        string = "LogitechLCDConfig {sdkPath: " + self.sdkPath + "}"
        return string
