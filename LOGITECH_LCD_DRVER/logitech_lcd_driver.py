from LOGITECH_LCD_DRVER.LOGITECH_LCD_SDK import LogitechLCDSDK, TYPE_COLOR, TYPE_MONO
from LOGITECH_LCD_DRVER.models.logitech_lcd_config import LogitechLCDConfig


class LogitechLcdDriver:
    logitechSDK: LogitechLCDSDK
    logitechLCDConfig: LogitechLCDConfig

    def __init__(self, config) -> None:
        self.logitechLCDConfig = config
        self.logitechSDK = LogitechLCDSDK(self.logitechLCDConfig.sdkPath)

    def start(self):
        connected = self.logitechSDK.connect("CS:GO")
        if connected:
            self.write("CS:GO LCD", 1, 1)
            return True
        return False

    def waiting(self) -> None:
        """Print text while idling."""
        self.clear()
        self.write("WAIT FOR MATCHES", 1, 1)

    def stop(self) -> None:
        self.logitechSDK.lcd_shutdown()

    def clear(self) -> None:
        self.logitechSDK.lcd_set_text(0, "")
        self.logitechSDK.lcd_set_text(1, "")
        self.logitechSDK.lcd_set_text(2, "")
        self.logitechSDK.lcd_set_text(3, "")

    def write(self, text, line, align):
        count = len(text)
        space_number_before_text_centred = 26 - count
        space_number_before_text_left = (26 - count) * 2
        if align == 0:  # Left
            self.logitechSDK.lcd_set_text(line, text)
        elif align == 1:  # center
            spaces = " " * space_number_before_text_centred
            self.logitechSDK.lcd_set_text(line, spaces + text)
        elif align == 2:
            spaces = space_number_before_text_left * " "
            self.logitechSDK.lcd_set_text(line, spaces + text)
        for i in range(line + 1, 4):
            self.logitechSDK.lcd_set_text(i, "")
        self.logitechSDK.lcd_update()

    def progessString(self, value):
        rest = int(value % 32) // 4
        table = [u'\u2588', u'\u2589', u'\u2590', u'\u2591', u'\u2592', u'\u2593', u'\u2594', u'\u2595', u'']
        return int(value // 32) * u'\u2588' + table[8 - rest]
