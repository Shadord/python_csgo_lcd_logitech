import time

from CSGO_DRIVER.csgo_driver import CSGODriver, RequestHandler
from ICUE_DRIVER.icue_driver import ICueDriver

from LOGITECH_LCD_DRVER.logitech_lcd_driver import LogitechLcdDriver
from program_config import ProgramConfig

CONFIG_FILE_NAME = "config.json"


class CSGOApp:
    config: ProgramConfig

    def __init__(self):
        self.config = ProgramConfig.load(CONFIG_FILE_NAME)
        print(self.config.iCueConfig)
        print(self.config.csgoConfig)

        self.csgoDriver = CSGODriver(self.config.csgoConfig, RequestHandler, self.csgo_event_handler)
        self.lcdDriver = LogitechLcdDriver(self.config.logitechLCDConfig)
        self.iCueDriver = ICueDriver(self.config.iCueConfig)

    def start(self):
        if self.iCueDriver.connect():
            print(time.asctime(), '-', 'ICue Driver started')
            self.iCueDriver.request_control()
        if self.lcdDriver.connect():
            print(time.asctime(), '-', 'LCD Driver starting')

        print(time.asctime(), '-', 'CS:GO Driver starting')
        self.csgoDriver.serve_forever()

    def stop(self):
        self.csgoDriver.server_close()
        print(time.asctime(), '-', 'CS:GO GSI server stopped')
        self.lcdDriver.stop()
        print(time.asctime(), '-', 'LCD Driver stopped')
        self.iCueDriver.release_control()
        print(time.asctime(), '-', 'ICue Driver stopped')

    @staticmethod
    def csgo_event_handler(type, game_state):
        print(type)


def main():
    program = CSGOApp()
    try:
        program.start()
    except (KeyboardInterrupt, SystemExit):
        pass

    program.stop()


main()
