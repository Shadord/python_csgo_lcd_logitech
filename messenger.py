# -*- coding: utf-8 -*-
"""
Messenger Thread.

@auteur: Darkness4
"""
from threading import Thread
from time import asctime, sleep, time
from LOGITECH_LCD_DRVER import GLCD_SDK


def progress(i: int) -> bytes:
    """
    Progress bar, for arduino 5px large.

    Parameters
    ----------
    i : int
        Select which character to send to Arduino.

    Returns
    -------
    bytes : Character send to Arduino.

    """
    switcher = {i <= 0: b"\x07",
                i == 1: b"\x02",
                i == 2: b"\x03",
                i == 3: b"\x04",
                i == 4: b"\x05",
                i >= 5: b"\x06"}
    return switcher[True]


class Messenger(Thread):
    """
    Give order to the arduino.

    Attributes
    ----------
    __armor : int
        Armor points.
    __health : int
        Health points.
    __kills : tuple
        Number of kills and heads.
    __money : int
        Money left.
    __refresh : bool
        Status of the refresher.
    __start : bool
        Status of Messenger.
    __status : string
        Status of the round.
    ser_arduino : Serial
        Serial class of the Arduino.

    Methods
    -------
    bomb_timer()
        Start a bomb timer.
    idle()
        Put Messenger on idle and write a message.
    run()
        Start the Thread and run Messenger.
    shutdown()
        Shutdown Messenger.
    write_player_stats()
        Write the player stats on Arduino.

    """
    __name = None
    __manche = None
    __armor = None
    __health = None
    __kills = None  # tuple (total kills - hs, hs)
    __money = None
    __refresh = False  # Order to refresh informations
    __start = True  # Order to start/stop
    __status = "None"

    def __init__(self) -> None:
        """Init save."""
        super(Messenger, self).__init__()
        GLCD_SDK.initDLL("C:\\Program Files\\Logitech Gaming Software\\SDK\\LCD\\x64\\LogitechLcd.dll")
        GLCD_SDK.LogiLcdInit("CS:GO", GLCD_SDK.TYPE_COLOR + GLCD_SDK.TYPE_MONO)
        self.write("CS:GO LCD", 1, 1)



    @property
    def armor(self) -> int:
        """Get the armor."""
        return self.__armor

    @armor.setter
    def armor(self, armor: int) -> None:
        """Set the armor."""
        self.__armor = armor

    @property
    def money(self) -> int:
        """Get the money."""
        return self.__money

    @money.setter
    def money(self, money: int) -> None:
        """Set the money."""
        self.__money = money

    @property
    def health(self) -> int:
        """Get the health."""
        return self.__health

    @health.setter
    def health(self, health: int) -> None:
        """Set the health."""
        self.__health = health

    @property
    def name(self) -> str:
        """Get the health."""
        return self.__name

    @name.setter
    def name(self, name: str) -> None:
        """Set the health."""
        self.__name = name

    @property
    def manche(self):
        """Get the health."""
        return self.__manche

    @manche.setter
    def manche(self, manche) -> None:
        """Set the health."""
        self.__manche = manche

    @property
    def status(self) -> str:
        """Get the status."""
        return self.__status

    @status.setter
    def status(self, status: str) -> None:
        """
        Change Messenger behavior.

        Available status:
        'None'
        'planted'
        '!Freezetime'
        'Freezetime'
        'defused'
        'exploded'
        """
        self.__status = status
        self.__refresh = True  # Informations need to be refreshed

    @property
    def kills(self) -> tuple:
        """Get the kills (K, HS)."""
        return self.__kills

    @kills.setter
    def kills(self, kills_heads: tuple) -> None:
        """Set the number of kills (K, HS)."""
        self.__kills = (int(kills_heads[0])-int(kills_heads[1]),
                        int(kills_heads[1]))

    def run(self) -> None:
        """Thread start."""
        while self.__start:
            if self.__refresh:
                self.__refresh = False  # Has refreshed
                print(self.status)
                if self.status in ("planted", "defused", "exploded"):  # Bomb
                    self.bomb_timer()

                elif self.status == "None":
                    self.idle()

                else:  # Default status
                    self.write_player_stats()
            else:
                sleep(0.3)  # Saving consumption
        print(asctime(), "-", "Messenger is dead.")

    def bomb_timer(self) -> None:
        """40 sec bomb timer on arduino."""
        offset = time()
        actual_time = 39 - (time() - offset)
        old_time = actual_time
        while actual_time > 0 and self.status == "planted":
            actual_time = 38 - (time() - offset)
            print(str(old_time) + " -- " + str(actual_time))
            if old_time >= actual_time + 0.5:  # Actualization only integer change
                old_time = actual_time
                self.write("BOMB PLANTED", 0, 1)
                self.write("TIME TO DIFFUSE = " + str(int(actual_time)), 1, 0)
        if self.status == "defused":
            self.write("BOMB DIFFUSED", 0, 1)
        elif self.status == "exploded":
            self.write("BOMB EXPLODED", 0, 1)

    def write_player_stats(self) -> None:
        """Player stats writer."""
        self.write("PLAYER " + self.name + " STAT", 0, 1)
        first_line = ""
        second_line = ""
        third_line = ""

        # Set Health + Armor with progress
        first_line = u'\u2764: ' + str(self.health) + self.progessString(self.health)
        second_line = u'\u26CA: ' + str(self.armor) + self.progessString(self.armor)

        # Kill or Money
        if self.status == "!Freezetime":
            # HS and Kill counter
            third_line = "K: " + str(self.kills[0]) + " -- HS: " + str(self.kills[1])
        # Not kill streak
        elif self.status == "Freezetime":
            third_line = "M: " + str(self.money) + "$"

        if (self.manche["ct"] + self.manche["t"] == 14) or self.manche["t"] == 15 or self.manche["ct"] == 15:
            third_line = third_line + u'\u2764' + "LAST"

        self.write(first_line, 1, 0)
        self.write(second_line, 2, 0)
        self.write(third_line, 3, 0)



        sleep(0.1)

    def idle(self) -> None:
        """Print text while idling."""
        self.clear()
        self.write("WAIT FOR MATCHES", 1, 1)

    def shutdown(self) -> None:
        """Stop Messenger."""
        self.__start = False
        GLCD_SDK.LogiLcdShutdown()

    def clear(self) -> None:
        GLCD_SDK.LogiLcdMonoSetText(0, "")
        GLCD_SDK.LogiLcdMonoSetText(1, "")
        GLCD_SDK.LogiLcdMonoSetText(2, "")
        GLCD_SDK.LogiLcdMonoSetText(3, "")

    @staticmethod
    def write(text, line, align):
        count = len(text)
        space_number_before_text_centred = 26 - count
        space_number_before_text_left = (26 - count)*2
        if align == 0: #Left
            GLCD_SDK.LogiLcdMonoSetText(line, text)
        elif align == 1: #center
            spaces = " " * space_number_before_text_centred
            GLCD_SDK.LogiLcdMonoSetText(line, spaces + text)
        elif align == 2:
            spaces = space_number_before_text_left * " "
            GLCD_SDK.LogiLcdMonoSetText(line, spaces + text)
        for i in range(line+1, 4):
            GLCD_SDK.LogiLcdMonoSetText(i, "")
        GLCD_SDK.LogiLcdUpdate()

    def progessString(self, value):
        rest = int(value%32)//4
        table = [u'\u2588', u'\u2589', u'\u2590', u'\u2591', u'\u2592', u'\u2593', u'\u2594', u'\u2595', u'']
        return int(value//32)*u'\u2588' + table[8 - rest]
