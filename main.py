# -*- coding: utf-8 -*-
"""
CSGO's informations displayed on an Arduino featuring a bomb timer.

@auteur: tsuriga, Darkness4
"""

from server import ServerThread

server_thread = None


def main():
    """Launch."""
    global server_thread

    if server_thread is None:
        server_thread = ServerThread()
    server_thread.run()
    while True:
        pass


main()
