import json

from CSGO_DRIVER.models.csgo_config import CSGOConfig
from ICUE_DRIVER.models.i_cue_config import ICueConfig
from LOGITECH_LCD_DRVER.models.logitech_lcd_config import LogitechLCDConfig


class ProgramConfig:
    iCueConfig: ICueConfig
    logitechLCDConfig: LogitechLCDConfig
    csgoConfig: CSGOConfig

    def __init__(self, i_cue_config, logitech_lcd_config, csgo_config):
        self.iCueConfig = i_cue_config
        self.logitechLCDConfig = logitech_lcd_config
        self.csgoConfig = csgo_config

    @classmethod
    def from_dict(cls, data):
        return cls(ICueConfig.from_dict(data['iCueConfig']), LogitechLCDConfig.from_dict(data['logitechLcdConfig']),
                   CSGOConfig.from_dict(data['csgoConfig']))

    @classmethod
    def load(cls, file):
        f = open(file, "r")
        data = json.load(f)
        return cls.from_dict(data)
