from ICUE_DRIVER.ICUE_SDK import ICueSdk
from ICUE_DRIVER.models.i_cue_config import ICueConfig
from ICUE_DRIVER.models.profile import Profile


class ProfileManager:
    iCueSdk: ICueSdk

    config: ICueConfig

    profiles: [Profile]

    lastTriggeredProfile: Profile

    def __init__(self, config):
        self.config = config
        self.iCueSdk = ICueSdk(self.config.sdkPath)
        self.profiles = self.load_profiles()

        if self.config.startProfileName != "":
            startProfile = self.get_profile(self.config.startProfileName)
            if (startProfile != None):
                self.activate_profile(startProfile, True)

    def load_profiles(self) -> {}:
        profile_priorities_path = self.config.gameSdkEffectsPath + "\\" + self.config.gameProfileDir + "\\" + "priorities.cfg"
        profiles = []
        priorities_file = open(profile_priorities_path, 'r')
        for row in priorities_file.readlines():
            key_and_value = row.split("=")
            if key_and_value[0] != self.config.autoSyncProfileName:
                profiles.append(Profile(key_and_value[0], int(key_and_value[1]), False))
        return profiles

    def get_profile(self, profile_name):
        for e in self.profiles:
            if e.name == profile_name:
                return e

    def trigger_profile(self, profile):
        profile.state = False
        self.lastTriggeredProfile = profile
