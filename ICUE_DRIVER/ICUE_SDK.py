from ctypes import *


class ICueSdk:
    def __init__(self, dll_path):
        self.dll_path = dll_path
        self._dll_file = CDLL(dll_path)

        self.clear_all_events = self._dll_file['CgSdkClearAllEvents']
        self.clear_all_events.restype = c_bool

        self.clear_all_states = self._dll_file['CgSdkClearAllStates']
        self.clear_all_states.restype = c_bool

        self.clear_state = self._dll_file['CgSdkClearState']
        self.clear_all_states.restype = c_bool
        self.clear_state.argtypes = [c_wchar_p]

        self.get_last_error = self._dll_file['CgSdkGetLastError']
        self.get_last_error.restype = c_int

        self.perform_protocol_handshake = self._dll_file['CgSdkPerformProtocolHandshake']
        self.get_last_error.restype = c_wchar_p

        self.release_control = self._dll_file['CgSdkReleaseControl']
        self.release_control.restype = c_bool

        self.request_control = self._dll_file['CgSdkRequestControl']
        self.request_control.restype = c_bool

        self.set_event = self._dll_file['CgSdkSetEvent']
        self.set_event.restype = c_bool
        self.set_event.argtypes = [c_wchar_p]

        self.set_game = self._dll_file['CgSdkSetGame']
        self.set_game.restype = c_bool
        self.set_game.argtypes = [c_wchar_p]

        self.set_state = self._dll_file['CgSdkSetState']
        self.set_state.restype = c_bool
        self.set_state.argtypes = [c_wchar_p]

        self.game = None
        self.corsairProtocolDetails = None

    def connect(self, game):
        self.game = game
        self.corsairProtocolDetails = self.perform_protocol_handshake()
        self.set_game(self.game)
