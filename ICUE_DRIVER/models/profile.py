class Profile:
    def __init__(self, name, priority, state):
        self.name = name
        self.priority = priority
        self.state = state

    def to_dict(self):
        dictionnary = {"name": self.name, "priority": self.priority,
                       "state": self.state}
        return dictionnary

    @classmethod
    def from_dict(cls, data):
        return cls(data["name"], data["priority"], data["state"])
