class ICueConfig:
    def __init__(self, sdk_path, game_sdk_effects_path, game_profile_dir, auto_sync_profile_name, auto_sync_interval, start_profile_name):
        self.sdkPath = sdk_path
        self.gameSdkEffectsPath = game_sdk_effects_path
        self.gameProfileDir = game_profile_dir
        self.autoSyncProfileName = auto_sync_profile_name
        self.autoSyncInterval = auto_sync_interval
        self.startProfileName = start_profile_name

    def to_dict(self):
        dictionnary = {"sdkPath": self.sdkPath, "gameSdkEffectsPath": self.gameSdkEffectsPath, "gameProfileDir": self.gameProfileDir, "autoSyncProfileName": self.autoSyncProfileName,
                       "autoSyncInterval": self.autoSyncInterval, "startProfileName": self.startProfileName}
        return dictionnary

    @classmethod
    def from_dict(cls, data):
        return cls(data['sdkPath'], data['gameSdkEffectsPath'], data["gameProfileDir"], data["autoSyncProfileName"], data["autoSyncInterval"],
                   data["startProfileName"])

    def __repr__(self):
        string = "ICueConfig {" \
                 "sdkPath: " + self.sdkPath + \
                 ", gameSdkEffectsPath: " + self.gameSdkEffectsPath + \
                 ", gameProfileDir: " + self.gameProfileDir + \
                 ", autoSyncProfileName: " + self.autoSyncProfileName + \
                 ", autoSyncInterval: " + str(self.autoSyncInterval) + \
                 ", startProfileName: " + self.startProfileName + "}"
        return string