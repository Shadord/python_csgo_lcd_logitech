# -*- coding: utf-8 -*-
"""
Server Thread.

@auteur: Darkness4
"""

from time import asctime, sleep

from httpserver import CSGORequestHandler, HTTPCSGOServer


class ServerThread:
    """
    Server's thread.

    Attributes
    ----------
    com_str : str
        COM Port in str.
    ser_arduino : Serial
        Status of Messenger.
    server : Server
        Status of the refresher.

    Methods
    -------
    run()
        Start the Thread and run Server.

    """

    server = None

    def run(self) -> None:
        """Start the server."""

        # Launch server
        self.server = HTTPCSGOServer(('127.0.0.1', 5000),
                                     CSGORequestHandler)
        print(asctime(), '-', 'CS:GO GSI Quick Start server starting on port 5000')
        self.server.serve_forever()  # Run
        self.server.server_close()  # Close server
        print(asctime(), '-', 'CS:GO GSI Quick Start server stopped')
        self.ser_arduino.close()  # Close COM port
        print(asctime(), '-', 'Serial stopped')
